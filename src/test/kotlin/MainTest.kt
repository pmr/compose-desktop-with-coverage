package com.example

import androidx.compose.ui.test.hasTestTag
import androidx.compose.ui.test.junit4.createComposeRule
import kotlin.test.Test
import org.junit.Rule

class MainTest {
    @get:Rule val composeTestRule = createComposeRule()

    @Test
    fun `When app is created verify hello world button exists`() {
        with(composeTestRule) {
            setContent { app() }
            onNode(hasTestTag(buttonTestTag), true).assertExists("Hello World button does not exist!")
        }
    }
}